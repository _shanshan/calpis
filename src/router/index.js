import Vue from 'vue'
import Router from 'vue-router'
// import Home from '@/components/home/Home'
// import Before_Gravity from '@/components/home/Before_Gravity'
// import Gravity from '@/components/home/Gravity'
// import StarPointIndex from '@/components/starPoint/Index'
// import StarPointStart from '@/components/starPoint/Start'
// import TakePhoto from '@/components/starPoint/TakePhoto'
// import DrawLine from '@/components/starPoint/DrawLine'
// import UserInfo from '@/components/starPoint/UserInfo'
// import UserInfo2 from '@/components/starPoint/UserInfo2'
// import Result from '@/components/starPoint/Result' 
// import Details from '@/components/starPoint/Details'
// import Share from '@/components/starPoint/Share'
// import Poster from '@/components/starPoint/Poster'


Vue.use(Router)
let _Router =  new Router({
    routes: [{
            path: '/',
            name: 'Loading',
            component: resolve => {
                require(['../components/home/Loading.vue'], resolve)
            }, //懒加载
        },
        {
            path: '/first',
            name: 'First',
            component: resolve => {
                require(['../components/home/First.vue'], resolve)
            }, //懒加载
        },
        {
            path: '/home',
            name: 'Home',
            component: resolve => {
                require(['../components/home/Home.vue'], resolve)
            }, //懒加载
        },
        {
            path: '/before_gravity',
            name: 'Before_Gravity',
            component: resolve => {
                require(['../components/home/Before_Gravity.vue'], resolve)
            }, //懒加载
        },
        {
            path: '/gravity2',
            name: 'Gravity2',
            component: resolve => {
                require(['../components/home/Gravity2.vue'], resolve)
            }, //懒加载
        },
        {
            path: '/gravity3',
            name: 'Gravity3',
            component: resolve => {
                require(['../components/home/Gravityleo.vue'], resolve)
            }, //懒加载
        },
        {
            path: '/starpoint',
            component: resolve => {
                require(['../components/starPoint/Index.vue'], resolve)
            }, //懒加载,
            redirect: '/starpoint/start',
            children: [{
                    path: 'start',
                    name: 'StarPointStart',
                    component: resolve => {
                        require(['../components/starPoint/Start.vue'], resolve)
                    }, //懒加载
                },
                {
                    path: 'takephoto',
                    name: 'TakePhoto',
                    component: resolve => {
                        require(['../components/starPoint/TakePhoto.vue'], resolve)
                    }, //懒加载
                },
                {
                    path: 'drawline',
                    name: 'DrawLine',
                    component: resolve => {
                        require(['../components/starPoint/DrawLine.vue'], resolve)
                    }, //懒加载
                },
                {
                    path: 'userinfo',
                    name: 'UserInfo',
                    component: resolve => {
                        require(['../components/starPoint/UserInfo.vue'], resolve)
                    }, //懒加载
                }, {
                    path: 'userinfo2',
                    name: 'UserInfo2',
                    component: resolve => {
                        require(['../components/starPoint/UserInfo2.vue'], resolve)
                    }, //懒加载
                },
                {
                    path: 'result',
                    name: 'Result',
                    component: resolve => {
                        require(['../components/starPoint/Result.vue'], resolve)
                    }, //懒加载
                },
                {
                    path: 'details',
                    name: 'Details',
                    component: resolve => {
                        require(['../components/starPoint/Details.vue'], resolve)
                    },
                },
                {
                    path: 'share',
                    name: 'Share',
                    component: resolve => {
                        require(['../components/starPoint/Share.vue'], resolve)
                    },
                },
                {
                    path: 'poster',
                    name: 'Poster',
                    component: resolve => {
                        require(['../components/starPoint/Poster.vue'], resolve)
                    },
                }
            ]
        }
    ]

})
// _Router.beforeEach((to, from, next)=>{
//   if (to.name === 'Share' || to.name === 'Poster') {
//     localStorage.setItem('showBGMICON',false)
//   }else{
//     localStorage.setItem('showBGMICON',true)
//   }
//   next();
// })

export default _Router