// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import $ from 'zepto'
import wxapis from "./libs/wxapi"


// 按需引入 
import 'vue-ydui/dist/ydui.base.css';
import { Toast ,Loading} from 'vue-ydui/dist/lib.px/dialog';

Vue.prototype.EXIF = EXIF

Vue.prototype.$dialog = {
  toast: Toast,
  loading:Loading
};

Vue.config.productionTip = false


let ua = navigator.userAgent.toLowerCase();
//系统判断
if(ua.match(/iPhone/i) == "iphone") {
  //iphone
  Vue.prototype._equipment = 'iphone'

}else if(ua.match(/Android/i) == "android"){
  //android
  Vue.prototype._equipment = 'android'
}else if(ua.match(/MicroMessenger/i)=="micromessenger") {
  //是微信
  Vue.prototype._equipment = 'wx'
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  data(){
    return{
      avatarIMG:'',
      isshowBGM:true
    }
  }
})
